from django_cron import CronJobBase, Schedule

class ResolveAuctions(CronJobBase):
    
    RUN_EVERY_MINS = 5 # every 1 min
    RETRY_AFTER_FAILURE_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'homepage.ResolveAuctions'    # a unique code

    def do(self):
        from django.core.mail import send_mail
        from django.conf import settings
        from .models import Auction
        from django.db import transaction
        import datetime
        import traceback
        
        
        now = datetime.datetime.now()
        with transaction.atomic():
            print("Running ResolveAuctions")
            try:
                auctions = Auction.objects.filter(deadline__lte=now, over=False)
                auctions = list(auctions)
                print("Resolving " + str(len(auctions)) + " auctions.")
                for auction in auctions:
                    auction1 = Auction.objects.filter(id=auction.id)[0]
                    auction1.over = True

                    bids = None
                    last_bid = None
                    email_winner = 'Nobody(No bids)'
                    if auction1.bid_set.count() > 0:
                        bids = auction1.bid_set.all().order_by('-id')
                        last_bid = bids[0]
                        winner = last_bid.bidder
                        auction1.winner = winner
                        email_winner = winner.username

                    auction1.save()

                    if auction1.bid_set.count() > 0 and bids:
                        for bid in bids:
                            send_mail(
                                'The auction  \"' + auction1.title + '\" is resolved.',
                                '<a href=\"https://' + settings.SITE_DOMAIN + settings.SITE_PORT + "/auction/" + str(auction1.id) + '\">Click here to view the auction</a>' +
                                '<br>The winner was ' + str(email_winner),
                                'auctioneer@' + settings.SITE_DOMAIN,
                                [bid.bidder.email],
                                fail_silently=False,
                            )


                    send_mail(
                        'The auction  \"' + auction1.title + '\" is resolved.',
                        '<a href=\"https://' + settings.SITE_DOMAIN + settings.SITE_PORT + "/auction/" + str(auction1.id) + '\">Click here to view the auction</a>' +
                        '<br>The winner was ' + email_winner,
                        'auctioneer@' + settings.SITE_DOMAIN,
                        [auction1.seller],
                        fail_silently=False,
                    )
                    


            except Exception as e:
                traceback.print_exc()
