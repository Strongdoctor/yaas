from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

class Auction(models.Model):
    seller = models.ForeignKey(User, on_delete=models.CASCADE, related_name='seller')
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=1024)
    minimum_price = models.DecimalField(max_digits=19, decimal_places=2)
    deadline = models.DateTimeField()
    creation = models.DateTimeField(auto_now_add=True)
    banned = models.BooleanField(default=False)
    winner = models.ForeignKey(User, null=True, related_name='winner')
    version = models.IntegerField(default=0)
    over = models.BooleanField(default=False)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    currency = models.CharField(max_length=8, default='EUR')
    language = models.CharField(max_length=3, default='en')

    def save(self, *args, **kwargs):
        if not self.pk:
            try:
                p = Profile.objects.get(user=self.user)
                self.pk = p.pk
            except Profile.DoesNotExist:
                pass

        super(Profile, self).save(*args, **kwargs)

class Bid(models.Model):
    bidder = models.ForeignKey(User, on_delete = models.CASCADE)
    auction = models.ForeignKey(Auction, on_delete = models.CASCADE)
    amount = models.DecimalField(max_digits=19, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, created, **kwargs):
    instance.profile.save()
    if created:
        Token.objects.create(user=instance)