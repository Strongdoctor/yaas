# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-17 16:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0009_auto_20171012_1415'),
    ]

    operations = [
        migrations.AddField(
            model_name='auction',
            name='over',
            field=models.BooleanField(default=False),
        ),
    ]
