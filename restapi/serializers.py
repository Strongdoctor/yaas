from homepage.models import Auction, Bid
from rest_framework import serializers
from django.contrib.auth.models import User, Group


class AuctionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Auction
        fields = ('id', 'url', 'seller', 'title', 'description', 'minimum_price', 'deadline', 'creation', 'banned', 'winner')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class BidSerializer(serializers.Serializer):
    auction_id = serializers.IntegerField()
    amount = serializers.DecimalField(max_digits=19, decimal_places=2)
    version = serializers.IntegerField()