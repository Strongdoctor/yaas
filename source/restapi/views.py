from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from homepage.models import Auction, Bid
from .serializers import AuctionSerializer, UserSerializer, GroupSerializer, BidSerializer
from django.contrib.auth.models import User, Group
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db import transaction
from django.core.mail import send_mail
from rest_framework.exceptions import NotFound
import json
import datetime

# The parameters mean we will only allow /auctions/1(RetrieveModel) and /auctions/()
class AuctionViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin, 
                     viewsets.GenericViewSet):
    serializer_class = AuctionSerializer
    queryset = Auction.objects.all()

    def list(self, request):
        permission_classes = (AllowAny,)

        # If user supplies proper search terms do this:
        print(request.GET.get('search', False))
        if request.GET.get('search', False) != False:
            search_term = request.GET.get('search', False)
            serializer = AuctionSerializer(self.queryset.filter(title__icontains=str(search_term)), many=True, context={'request': request})
            print(serializer.data)
            if serializer.data:
                return Response(serializer.data)
            else:
                raise NotFound(detail="No auction found.", code=404)
        else:
            serializer = AuctionSerializer(self.queryset, many=True, context={'request': request})
            return Response(serializer.data)

class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class BidViewSet(mixins.CreateModelMixin,
                 viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)
    #authentication_classes = (TokenAuthentication,)
    queryset = Bid.objects.all()
    serializer_class = BidSerializer
    def create(self, request):

        authentication_classes = (TokenAuthentication,)
        parser_classes = (JSONParser,)
        serializer = BidSerializer(data=request.data)

        response_json = dict()
        errors = list()

        if serializer.is_valid():
            print(serializer.validated_data)
            with transaction.atomic():
                try:
                    auction = Auction.objects.select_for_update().get(pk=serializer.data['auction_id'])
                except Auction.DoesNotExist as e:
                    print(e)
                    errors.append({"type": "auction_not_found", "msg": "Auction with that id not found."})
                    response_json['errors'] = errors
                    return Response(response_json['errors'])

                last_bid = None
                if auction.bid_set.count() > 0:
                    last_bid =  auction.bid_set.all().order_by('-id')[0]
                
                now = datetime.datetime.now()
                if request.user.username == auction.seller.username:
                    errors.append({"type": "bidder_is_seller", "msg": "You are the seller of this item and can therefore not bid on it."})

                if auction.bid_set.all().count() > 0:
                    if request.user.username == last_bid.bidder.username:
                        errors.append({"type": "latest_bidder", "msg": "You are the latest bidder."})

                if auction.banned:
                    errors.append({"type": "auction_banned", "msg": "The auction is banned. If you want to know why, contact the site administrator."})
                
                if now > auction.deadline or type(auction.winner) == User:
                    errors.append({"type": "deadline_passed", "msg": "The auction is over."})
                    if type(auction.winner) != User:
                        auction.winner = last_bid.bidder

                bid_amount = serializer.validated_data['amount']

                if auction.bid_set.count() > 0:
                    if (bid_amount <= last_bid.amount) or (bid_amount <= auction.minimum_price):
                        print(auction.bid_set.all().order_by('-id')[auction.id].amount)
                        errors.append({"type": "bid_low", "msg": "Bid amount too low."})
                else:
                    if bid_amount <= auction.minimum_price:
                        errors.append({"type": "bid_low", "msg": "Bid amount too low."})

                if serializer.validated_data['version'] != auction.version:
                    print("Serial: " + str(serializer.validated_data['version']) + " DB: " + str(auction.version))
                    errors.append({"type": "version_mismatch", "msg": "Wrong version. The auction may have been updated, please refresh the browser"})

                if errors:
                    response_json['errors'] = errors
                    print(response_json)
                    return Response(response_json)
                else:
                    bid = Bid.objects.select_for_update().create(
                                        bidder = User.objects.get(username=request.user.username),
                                        auction = auction,
                                        amount = bid_amount) 
                    auction.version = auction.version + 1

                    if(((auction.deadline - now).total_seconds() / 60) < 5):
                        auction.deadline = now + 5

                    auction.save()
                    response_json['success'] = "Successfully bid on the auction!"

                    # Send mail to last bidder if there is one
                    if auction.bid_set.all().count() > 0:
                        last_bidder = auction.bid_set.all().order_by('-id')[0].bidder
                        send_mail(
                            'You were outbid on auction \"' + auction.title + '\"',
                            '<a href=\"https://' + settings.SITE_DOMAIN + settings.SITE_PORT + "/" + str(auction.id) + '\">Click here to view the auction</a>',
                            'auctioneer@' + settings.SITE_DOMAIN,
                            [last_bidder.email],
                            fail_silently=False,
                        )

                    send_mail(
                        'Your auction \"' + auction.title + '\" has a new bid',
                        '<a href=\"https://' + settings.SITE_DOMAIN + settings.SITE_PORT + "/" + str(auction.id) + '\">Click here to view the auction</a>',
                        'auctioneer@' + settings.SITE_DOMAIN,
                        [auction.seller.email],
                        fail_silently=False,
                    )
                    send_mail(
                        'You have bid on \"' + auction.title + '\" ',
                        '<a href=\"https://' + settings.SITE_DOMAIN + settings.SITE_PORT + "/" + str(auction.id) + '\">Click here to view the auction</a>',
                        'auctioneer@' + settings.SITE_DOMAIN,
                        [request.user.email],
                        fail_silently=False,
                    )
                    print(response_json)

                return Response(response_json)
        else:
            return Response(serializer.errors)