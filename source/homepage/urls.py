from django.conf.urls import url
from . import views
from . import models

urlpatterns = [
    url(r'^$', views.homepage, name='home'),
    url(r'^language/$', views.language, name='language'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^profile/settings/$', views.settings, name='settings'),
    url(r'^profile/newauction/$', views.newauction, name='newauction'),
    url(r'^profile/newauction/confirm/$', views.confirmnewauction, name='confirm'),
    url(r'^auction/([0-9]+)$', views.auction, name='auction'),
    url(r'^auction/([0-9]+)/edit/$', views.editauction, name='editauction'),
]
