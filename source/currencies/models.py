from django.db import models

class CurrenciesManager(models.Manager):
    def create_currencies(self, id, currencies_json):
        currencies = self.create(id=id, currencies_json=currencies_json)
        return currencies

class Currencies(models.Model):
    id = models.IntegerField(primary_key=True)
    currencies_json = models.CharField(max_length=2048)
    objects = CurrenciesManager()