from django.contrib import admin
from .models import Auction

class AuctionAdmin(admin.ModelAdmin):
	list_display = ('title', 'minimum_price', 'deadline', 'creation', 'banned')

admin.site.register(Auction, AuctionAdmin)
