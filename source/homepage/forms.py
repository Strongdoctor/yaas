from django import forms
from django.contrib.auth.models import User
import datetime
import decimal

class UserSettingsForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput, max_length=255, label="New password:", required=False)
    new_password_confirm = forms.CharField(widget=forms.PasswordInput, max_length=255, label="Confirm new password:", required=False)
    new_email = forms.CharField(max_length=255, label="New account email", required=False)
    new_email_confirm = forms.CharField(max_length=255, required=False)
    currencies = (
        ('EUR', 'Euro'),
        ('USD', 'United States dollar'),
        ('SEK', 'Swedish Crowns')
    )
    currency = forms.ChoiceField(choices=currencies)
    current_password = forms.CharField(widget=forms.PasswordInput, max_length=255, label="Current password:")

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(UserSettingsForm, self).__init__(*args, **kwargs)
        self.initial['currency'] = self.user.profile.currency

    def clean(self):
        cleaned_data = super(UserSettingsForm, self).clean()
        new_password = cleaned_data.get("new_password")
        new_password_confirm = cleaned_data.get("new_password_confirm")
        new_email = cleaned_data.get("new_email")
        new_email_confirm = cleaned_data.get("new_email_confirm")
        new_currency = cleaned_data.get("currency")
        current_password = cleaned_data.get("current_password")

        #Checks if there's input in new_password or new_password_confirm, and in that case if they contain the same values.

        if ((new_password or new_password_confirm) and
            (new_password != new_password_confirm)):
            self.add_error('new_password_confirm', "The passwords do not match:")


        if User.objects.filter(email=new_email).exists():
            self.add_error('new_email', "A user with that email already exists")

        if ((new_email or new_email_confirm) and
            (new_email != new_email_confirm)):
            self.add_error('new_email_confirm', "The email addresses do not match")

        if not any(new_currency in currency for currency in self.currencies):
            self.add_error('currency', "Invalid currency")

        return cleaned_data

class NewAuctionForm(forms.Form):
    title = forms.CharField(max_length=255)
    description = forms.CharField(widget=forms.Textarea, max_length=1024)
    minimum_price = forms.DecimalField(max_digits=19, decimal_places=2)
    deadline = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M'], label="Auction end date(yyyy-mm-dd hh:mm)")

    def clean(self):
        cleaned_data = super(NewAuctionForm, self).clean()
        minimum_price = cleaned_data.get("minimum_price")
        deadline = cleaned_data.get("deadline")

        if type(minimum_price) is decimal.Decimal:
            if minimum_price < decimal.Decimal('0.00'):
                self.add_error('minimum_price', "Too low minimum price, please keep it positive.")
        else:
            self.add_error('minimum_price', "No minimum price specified.")

        if type(deadline) is datetime.datetime:
            if deadline < datetime.datetime.now() + datetime.timedelta(hours=72):
                self.add_error('deadline', "Deadline would be before right now. Nice try.")
        else:
            self.add_error('deadline', "No deadline specified")

        return cleaned_data

class EditAuctionForm(forms.Form):
    description = forms.CharField(widget=forms.Textarea, max_length=1024)
