function makeBid(id, amount, ver, token) {
    $.ajax({
        url: '/api/bid/',
        type: 'post',
        data: {
            auction_id: id,
            amount: amount,
            version: ver
        },
        headers: {
            Authorization: 'Token '.concat(token)
        },
        dataType: 'json',
        success: function (data) {
            console.info(data);
            if(typeof(data['success']) == 'string') {
                document.location.reload(true)
            } else {
                console.error(data['errors'][0])

                // Handles an eventual version mismatch.
                var i = false;
                for(x = 0; x < data['errors'].length; x++) {
                    if (data['errors'][x]['type'] == "version_mismatch") {
                        i = x;
                    }
                }
                console.log(i)

                if(i !== false) {
                    sessionStorage.setItem('tempmsg', data['errors'][i]['msg'])
                    document.location.reload(true)
                }
                // End of the handling of version mismatch 

                document.getElementById('messages').innerHTML = JSON.stringify(data);
            }
        }
    });
}

function searchForAuction(search_term) {
    if (search_term.length > 0) {
        $.ajax({
        url: '/api/auctions/',
        type: 'get',
        dataType: 'json',
        data: {
            'search': search_term
        },
        success: function (data) {
            if(data.length > 0) {
                text = ""
                for(i = 0; i < data.length; i++) {
                    text += "<li class=\"auction front\"><a href=\"/auction/" + data[i]['id'] +"\">" + data[i]['title'] + "</a></li>"
                }
                document.getElementById('searched_auctions').innerHTML = text
            } else {
                document.getElementById('searched_auctions').innerHTML = "No auctions found."
            }
        },
        error:function(data, ajaxoptions, thrownerror) {
            if(data.status==404) {
                document.getElementById('searched_auctions').innerHTML = "No auctions found."
            }
        }
    });
    } else {
        $.ajax({
        url: '/api/auctions/',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            if(data.length > 0) {
                text = ""
                for(i = 0; i < data.length; i++) {
                    text += "<li class=\"auction front\"><a href=\"/auction/" + data[i]['id'] +"\">" + data[i]['title'] + "</a></li>"
                }
                document.getElementById('searched_auctions').innerHTML = text
            } else {
                document.getElementById('searched_auctions').innerHTML = "No auctions found."
            }
        },
        error:function(data, ajaxoptions, thrownerror) {
            if(data.status==404) {
                document.getElementById('searched_auctions').innerHTML = "No auctions found."
            }
        }
    });
    }

}

function popTempMsg() {
    document.getElementById('messages').innerHTML = sessionStorage.getItem('tempmsg')
    sessionStorage.removeItem('tempmsg')
}