from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from .forms import LoginForm, RegistrationForm
from django.utils import translation
from django.contrib.auth.models import User

def login(request):
    if request.method == 'GET':
        form = LoginForm()
        if request.user.is_authenticated:
            return redirect('/profile')
        return render(request, 'homepage/login.html', {'pagetitle': 'Login', 'form': form})

    elif request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                translation.activate(user.profile.language)
                request.session[translation.LANGUAGE_SESSION_KEY] = user.profile.language
                auth_login(request, user)
            else:
                form.add_error(None, "Invalid username or password")   
                return render(request, 'homepage/login.html', {'pagetitle': 'Login', 'form': form})
            return redirect('/profile')
        else:
            form.add_error(None, "Invalid username or password")  
       
        return render(request, 'homepage/login.html', {'pagetitle': 'Login', 'form': form})

def logout(request):
    if request.user.is_authenticated:
        auth_logout(request)
    return redirect('/login')

def register(request):
    if request.method == 'GET':
        form = RegistrationForm()
        return render(request, 'homepage/register.html', {'pagetitle': 'Register', 'form': form})
    elif request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            password_confirm = request.POST['password_confirm']
            email = request.POST['email']
            email_confirm = request.POST['email_confirm']
            user = User.objects.create_user(username, email, password)
            user.save()
            auth_login(request, user)
            return redirect('/profile')

        return render(request, 'homepage/register.html', {'pagetitle': 'Register', 'form': form})