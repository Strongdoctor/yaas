from django_cron import CronJobBase, Schedule
from .models import Currencies
from currencies import converter
from django.db import transaction

class UpdateCurrencies(CronJobBase):
    RUN_EVERY_MINS = 60 # every hour
    RETRY_AFTER_FAILURE_MINS = 5

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS, retry_after_failure_mins=RETRY_AFTER_FAILURE_MINS)
    code = 'currencies.UpdateCurrencies'    # a unique code

    def do(self):
        currencies.currencies_json = converter.fetch_currencies_json()
        with transaction.atomic():
        	currencies = Currencies.objects.select_for_update().get(id=0)
        	currencies.save()