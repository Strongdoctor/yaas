from django.shortcuts import render, redirect
from .forms import UserSettingsForm, NewAuctionForm, EditAuctionForm
from .models import Auction, Bid
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db import transaction
from django.utils import translation
from django.utils.translation import ugettext as _
from currencies import converter
from django.http import Http404
from rest_framework.authtoken.models import Token
import datetime


def homepage(request):
    if request.method == 'GET':
        return render(request, 'homepage/home.html', {'pagetitle': _('Home')})


def language(request):
    chosen_lang = request.GET.get('language')
    if not request.user.is_authenticated:
        translation.activate(chosen_lang)
        request.session[translation.LANGUAGE_SESSION_KEY] = chosen_lang
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        user = User.objects.get(username=request.user.username)
        user.profile.language = chosen_lang
        user.save()
        translation.activate(chosen_lang)
        request.session[translation.LANGUAGE_SESSION_KEY] = chosen_lang
        return redirect(request.META.get('HTTP_REFERER'))
        
def profile(request):
    if not request.user.is_authenticated:
        return redirect('/login')

    auctions = User.objects.get(username=request.user.username).seller.all()
    return render(request, 'homepage/profile.html', {'pagetitle': 'Profile', 'auctions': auctions})

def settings(request):
    if not request.user.is_authenticated:
        return redirect('/login')

    if request.method == 'GET':
        usersettingsform = UserSettingsForm(user=request.user)
        return render(request, 'homepage/settings.html', {'pagetitle': 'Settings', 'usersettingsform': usersettingsform})

    elif request.method == 'POST':
        usersettingsform = UserSettingsForm(request.POST, user=request.user)
        if usersettingsform.is_valid():
            # We don't need new_password_confirm or new_email_confirm as we've already validated that in forms.py
            new_password = request.POST.get('new_password', False)
            new_email = request.POST.get('new_email', False)
            new_currency = request.POST.get('currency')
            current_password = request.POST['current_password']
            user = authenticate(request, username=request.user.username, password=current_password)

            old_currency = user.profile.currency

            changedSettings = {}
            #Change the user's details if everything checks out
            if user is not None:
                if new_password:
                    user.set_password(new_password)
                    messages.add_message(request, messages.SUCCESS, "Successfully changed your password")

                if new_email:
                    user.email = new_email
                    messages.add_message(request, messages.SUCCESS, "Successfully changed your email")

                if new_currency != old_currency:
                    user.profile.currency = new_currency
                    messages.add_message(request, messages.SUCCESS, "Successfully updated your preferred currency")


                user.save()
                messages.add_message(request, messages.SUCCESS, "Currency set to: " + new_currency)
                return redirect('/login')

            else:
                usersettingsform.add_error('current_password', "Invalid password")
                return render(request, 'homepage/settings.html', {'pagetitle': 'Settings', 'usersettingsform': usersettingsform})
            return render(request, 'homepage/settings.html', {'pagetitle': 'Settings', 'usersettingsform': usersettingsform})
        return render(request, 'homepage/settings.html', {'pagetitle': 'Settings', 'usersettingsform': usersettingsform})

def newauction(request):
    if not request.user.is_authenticated:
        return redirect('/login')

    if request.method == 'GET':
        newauctionform = NewAuctionForm()
        return render(request, 'homepage/newauction.html', {'pagetitle': 'New Auction', 'newauctionform': newauctionform})
    elif request.method == 'POST':
        newauctionform = NewAuctionForm(request.POST)
        if newauctionform.is_valid():
            request.session['seller'] = request.user.username
            request.session['title'] = request.POST['title']
            request.session['description'] = request.POST['description']
            request.session['minimum_price'] = request.POST['minimum_price']
            request.session['deadline'] = request.POST['deadline']

            return redirect('confirm')
        else:
            return render(request, 'homepage/newauction.html', {'pagetitle': 'New Auction', 'newauctionform': newauctionform})

def confirmnewauction(request):
    if not request.user.is_authenticated:
        return redirect('/login')

    if request.method == 'GET':
        auctioninfo = {
            'title': request.session['title'],
            'description': request.session['description'],
            'minimum_price': request.session['minimum_price'],
            'deadline': request.session['deadline']
        }
        return render(request, 'homepage/confirmnewauction.html', {'pagetitle': 'Confirm New Auction', 'auctioninfo': auctioninfo})

    elif request.method == 'POST':
        if 'yesbutton' in request.POST:
            deadline = datetime.datetime.strptime(request.session['deadline'], '%Y-%m-%d %H:%M')
            if deadline < datetime.datetime.now():
                messages.add_message(request, messages.ERROR, "The deadline you selected has now passed, the auction was not created.")
                return redirect('/profile/newauction')

            seller = User.objects.get(username=request.session['seller'])
            minimum_price = converter.convert(request.session['minimum_price'], request.user.profile.currency, 'EUR').split(" ")[0]
            auction = Auction.objects.create(
                seller=seller,
                title=request.session['title'],
                description= request.session['description'],
                minimum_price=minimum_price,
                deadline=deadline
            )

            messages.add_message(request, messages.SUCCESS, "Successfully created your auction")

            send_mail(
                'Auction Creation',
                'Your auction was created at '+request.build_absolute_uri('/')+"auction/"+str(auction.pk),
                'local@ho.st',
                [seller.email],
                fail_silently=False,
            )

            del request.session['seller']
            del request.session['title']
            del request.session['description']
            del request.session['minimum_price']
            del request.session['deadline']

            return redirect('/profile')
        elif 'nobutton' in request.POST:
            messages.add_message(request, messages.INFO, "Your auction was not created")

            del request.session['seller']
            del request.session['title']
            del request.session['description']
            del request.session['minimum_price']
            del request.session['deadline']

            return redirect('/profile/newauction')

        # Removing all the session variables for security reasons

        del request.session['seller']
        del request.session['title']
        del request.session['description']
        del request.session['minimum_price']
        del request.session['deadline']

        return redirect('/profile')

def auction(request, auction_number):
    auction = Auction.objects.get(pk=auction_number)

    try:
        rest_token = Token.objects.get(user=request.user)
    except TypeError as e:
        print(e)
        rest_token = None

    if request.method == 'GET':
        curr_bid = None
        try: 
            curr_bid = auction.bid_set.all().order_by('-id')[0]
        except:
            pass

        return render(request, 'homepage/auction.html', {'pagetitle': auction.title, 'auction': auction, 'token': rest_token, 'current_bid': curr_bid})
    elif request.method == 'POST':
        if request.user.is_authenticated:
            if 'banauctionbutton' in request.POST:
                if request.user.is_staff:
                    with transaction.atomic():
                        auction = Auction.objects.select_for_update().get(pk=auction_number)
                        auction.banned = True
                        auction.save()
                    return redirect('/auction/' + auction_number)

            elif 'unbanauctionbutton' in request.POST:
                if request.user.is_staff:
                    with transaction.atomic():
                        auction = Auction.objects.select_for_update().get(pk=auction_number)
                        auction.banned = False
                        auction.save()
                    return redirect('/auction/' + auction_number)

def editauction(request, auction_number):
    auction = Auction.objects.get(pk=auction_number)
    if auction.seller != request.user:
        return redirect('/auction/'+auction_number)

    if request.method == 'GET':
        auction = Auction.objects.get(pk=auction_number)
        editauctionform = EditAuctionForm()
        editauctionform.fields['description'].initial= auction.description
        return render(request, 'homepage/editauction.html', {'pagetitle': 'Editing ' + auction.title, 'auction': auction, 'editauctionform': editauctionform})
    elif request.method == 'POST':
        auction = Auction.objects.get(pk=auction_number)
        editauctionform = EditAuctionForm(request.POST)
        if editauctionform.is_valid():
            auction.description = request.POST['description']
            auction.version = auction.version + 1
            auction.save()
            messages.add_message(request, messages.SUCCESS, "Your auction was successfully modified")
            return redirect('/auction/' + auction_number)

        return render(request, 'homepage/editauction.html', {'pagetitle': 'Editing ' + auction.title, 'auction': auction, 'editauctionform': editauctionform})
