from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from homepage.models import Auction, Bid
from decimal import *
import datetime
import random

class Command(BaseCommand):
    help = 'Generates a bunch of data for manage.py\'s dumpdata'

    def handle(self, *args, **kwargs):
        for x in range(0, 50):
            user = User.objects.create_user(
                username='userperson'+str(x),
                email='userperson'+str(x)+'@local.host',
                password='asdf2QWE'+str(x)
            )
            user.save()

            auction = Auction.objects.create(
                seller=user,
                title='Auction' + str(x),
                description= 'Auction' + str(x) + ' description',
                minimum_price=Decimal(x),
                deadline=datetime.datetime.strptime('2017-11-01 12:30', '%Y-%m-%d %H:%M')
            )
            auction.save()

            self.stdout.write(self.style.SUCCESS('Creating users and auctions (' + str(x+1) + '/50)'))

        for x in range(0, 10):
            auction = Auction.objects.all()
            bid = Bid.objects.select_for_update().create(
                bidder = user,
                auction = Auction.objects.get(id=random.randint(0,49)),
                amount = Decimal(x+3)
            )
            bid.save()

            self.stdout.write(self.style.SUCCESS('Creating bids (' + str(x+1) + '/10)'))