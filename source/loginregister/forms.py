from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(widget=forms.PasswordInput, max_length=255)

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(widget=forms.PasswordInput, max_length=255)
    password_confirm = forms.CharField(widget=forms.PasswordInput, max_length=255)
    email = forms.CharField(max_length=255)
    email_confirm = forms.CharField(max_length=255)

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()

        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        password_confirm = cleaned_data.get("password_confirm")
        email = cleaned_data.get("email")
        email_confirm = cleaned_data.get("email_confirm")

        if User.objects.filter(username=username).exists():
            self.add_error('username', "A user with that username already exists")

        if User.objects.filter(email=email).exists():
            self.add_error('email', "A user with that email already exists")

        if password_confirm != password:
            self.add_error('password_confirm', "The passwords do not match")

        if email_confirm != email:
            self.add_error('email_confirm', "The emails do not match")

        return cleaned_data