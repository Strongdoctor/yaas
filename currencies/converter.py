import requests
import json
import threading
from decimal import *
from .models import Currencies

def fetch_currencies_json():
    response = requests.get('https://openexchangerates.org/api/latest.json?app_id=2190ecfecadc4d769f2dc652f8b6ee40&symbols=EUR,SEK&prettyprint=false')
    return response.text

def parse_currencies(json_text):
    getcontext().prec = 32
    rates = json.loads(json_text)["rates"]
    for k, v in rates.items():
        rates[k] = Decimal(rates[k])
    return rates

def convert(value, source_symbol, dest_symbol):
    getcontext().prec = 32
    rates = parse_currencies(Currencies.objects.get(id=0).currencies_json)
    value = Decimal(value)
    #round up the value to 2 decimal accuracy
    if source_symbol == dest_symbol:
        return str(value) + " " + source_symbol

    #If source is USD
    if dest_symbol == 'USD':
        value_usd = value / rates[source_symbol]
        return str(value_usd.quantize(Decimal('.01'))) + " " + dest_symbol
    elif source_symbol == 'USD':
        value_converted = value * rates[dest_symbol]
        return str(value_converted.quantize(Decimal('.01'))) + " " + dest_symbol
    else:
        value_usd = value/rates[source_symbol]
        value_converted = value_usd*rates[dest_symbol]

        #print("Value_USD: " + str(value_usd) + " Converted: " + str(value_converted) + " Final: " + str(value_converted.quantize(Decimal('.01'))))
        return str(value_converted.quantize(Decimal('.01'))) + " " + dest_symbol