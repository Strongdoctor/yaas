from django import template
from currencies import converter

register = template.Library()

@register.simple_tag
def exchange_currency(value, source_symbol, dest_symbol, loggedin):
	if loggedin:
		return converter.convert(value, source_symbol, dest_symbol)
	else:
		return str(value) + " " + "EUR"
	

