from django.test import TestCase
from homepage.forms import NewAuctionForm
from decimal import *
from homepage.models import Auction
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import datetime
import random
import requests
import json

class NewAuctionFormTestCase(TestCase):
    def test_newauctionform_valid_data(self):
        form = NewAuctionForm({
            'title': "This is a title",
            'description': "This is a description",
            'minimum_price': Decimal(23),
            'deadline': datetime.datetime.strptime('2019-01-01 12:30', '%Y-%m-%d %H:%M')
        })

        self.assertTrue(form.is_valid())

    def test_newauctionform_blank_data(self):
        form = NewAuctionForm({
            'title': None,
            'description': None,
            'minimum_price': None,
            'deadline': None
        })

        self.assertFalse(form.is_valid())

    def test_newauctionform_invalid_minimum_price(self):
        form = NewAuctionForm({
            'title': "Reeee",
            'description': "Kek",
            'minimum_price': "asdf",
            'deadline': datetime.datetime.strptime('2019-01-01 12:30', '%Y-%m-%d %H:%M')
        })

        self.assertFalse(form.is_valid())

    def test_newauctionform_invalid_deadline1(self):
        form = NewAuctionForm({
            'title': "This is a title",
            'description': "This is a description",
            'minimum_price': Decimal(23),
            'deadline': (datetime.datetime.now() + datetime.timedelta(hours=70)).strftime('%Y-%m-%d %H:%M')
        })

        self.assertFalse(form.is_valid())

    def test_newauctionform_invalid_deadline2(self):
        form = NewAuctionForm({
            'title': "This is a title",
            'description': "This is a description",
            'minimum_price': Decimal(23),
            'deadline': "kekkyrek"
        })

        self.assertFalse(form.is_valid())

class CreateNewAuctionTestCase(TestCase):
    fixtures = ['fixtures',]

    def test_newauction_valid_data(self):
        """ Test creating valid auctions and validating the seller """

        for x in range(0,50):

            user = User.objects.get(pk=random.randint(1,50))
            auction = Auction.objects.create(
                seller=user,
                title="Titley",
                description="Descriptioney",
                minimum_price=Decimal(5),
                deadline=datetime.datetime.strptime('2019-01-01 12:30', '%Y-%m-%d %H:%M')
            )

            self.assertEqual(Auction.objects.filter(seller=user)[0].pk, auction.seller.pk)

class CreateNewBidTestCase(TestCase):
    fixtures = ['fixtures',]

    def test_newbid_invalid_auth(self):
        auction = Auction.objects.get(pk=random.randint(1,50))
        user = User.objects.get(pk=random.randint(1,50))
        bid_json = dict()
        bid_json['auction_id'] = auction.id
        bid_json['amount'] = 5000
        bid_json['version'] = auction.version+1

        headers = {'Authorization': 'Token ' + str(Token.objects.get(user=user).key)}
        print("HEADERS: " + str(headers))

        # SERVER NEEDS TO RUN FOR THIS!!!! #
        r = requests.post("http://localhost:8000/api/bid/", data=bid_json, headers=headers)

        # This is supposed to fail, as the user only exists locally in this test
        self.assertEqual(r.status_code, 401)
        self.assertEqual(json.loads(r.text)["detail"], "Invalid token.")