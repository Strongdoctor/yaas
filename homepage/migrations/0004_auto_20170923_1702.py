# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-23 17:02
from __future__ import unicode_literals

from decimal import Decimal
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0003_auto_20170923_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='auction',
            name='current_bidder',
            field=models.CharField(default=None, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='auction',
            name='current_bid',
            field=models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=19),
        ),
    ]
